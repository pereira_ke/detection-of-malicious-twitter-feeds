

def remove_duplicate_lines(inp,out):
	"""Function to remove duplicate lines from a text file and save output to different file
	inp = name of input file as a string with the extension e.g. 'inp.txt'
	out = name of output file as a string with the extension e.g. 'out.txt'
	"""
	unique_lines = []
	
	with open(inp) as f:
	    for line in f:
	    	if line not in unique_lines:
	    		unique_lines.append(line)
	
	with open(out,'w') as f:
		for line in unique_lines:
			f.write(line)


#Sample Usage:
#remove_duplicate_lines('inp.txt','out.txt')